<?xml version="1.0" encoding="UTF-8"?><?xml-model href="https://raw.githubusercontent.com/lombardpress/lombardpress-schema/develop/src/out/critical.rng" type="application/xml" schematypens="http://relaxng.org/ns/structure/1.0"?><?xml-model href="https://raw.githubusercontent.com/lombardpress/lombardpress-schema/develop/src/out/critical.rng" type="application/xml" schematypens="http://purl.oclc.org/dsdl/schematron"?><TEI xmlns="http://www.tei-c.org/ns/1.0">
  <teiHeader>
    <fileDesc>
      <titleStmt>
        <title>I, P. 1, Inq. 1, Tract. 5, S. 2, Q. 1, C. 7</title>
        <author ref="#AlexanderOfHales">Alexander of Hales</author>
        <respStmt>
          <name xml:id="JW">Jeffrey C. Witt</name>
          <resp>TEI encoder</resp>
        </respStmt>
      </titleStmt>
      <editionStmt>
        <edition n="0.0.0-dev">
          <date when="2017-03-10">March 10, 2017</date>
        </edition>
      </editionStmt>
      <publicationStmt>
        <authority>
          <ref target="http://lydiaschumacher.wixsite.com/earlyfranciscans">ERC Project 714427: Authority and Innovation in Early Franciscan Thought</ref>
        </authority>
        <availability status="free">
          <p>Published under a
            <ref target="http://creativecommons.org/licenses/by-nc-nd/3.0/">Creative Commons Attribution-NonCommercial-NoDerivs 3.0 License</ref>
          </p>
        </availability>
      </publicationStmt>
      <sourceDesc>
        <listWit>
          <witness xml:id="Q" n="quaracchi1924">Quaracchi 1924</witness>
          <witness xml:id="V" n="vatlat701">Vat lat 701</witness>
          <witness xml:id="B" n="borghlat359">Borgh. lat 359</witness>
          <witness xml:id="U" n="urblat123">Urb lat 123</witness>
          <witness xml:id="Pa" n="bnf15331">BnF 15331</witness>
        </listWit>
      </sourceDesc>
    </fileDesc>
    <encodingDesc>
      <schemaRef n="lbp-critical-1.0.0" url="https://raw.githubusercontent.com/lombardpress/lombardpress-schema/develop/src/out/critical.rng"/>
      <editorialDecl>
        <p>Encoding of this text has followed the recommendations of the LombardPress 1.0.0
          guidelines for a critical edition.</p>
      </editorialDecl>
    </encodingDesc>
    <revisionDesc status="draft">
      <listChange>
        <change when="2017-03-10" status="draft" n="0.0.0">
          <p>Created file for the first time.</p>
        </change>
      </listChange>
    </revisionDesc>
  </teiHeader>
  <text xml:lang="la">
    <front>
      
      <div xml:id="starts-on">
        <pb ed="#Q" n="272"/><cb ed="#Q" n="a"/>
      </div>
    </front>
    <body>
      <div xml:id="ahsh-l1p1i1t5s2q1c7">
        <head xml:id="ahsh-l1p1i1t5s2q1c7-Hd1e3741">I, P. 1, Inq. 1, Tract. 5, S. 2, Q. 1, C. 7</head>
        <head xml:id="ahsh-l1p1i1t5s2q1c7-Hd1e3744" type="question-title">UTRUM DIVINA PRAESCIENTIA SIT MUTABILIS.</head>
        <p xml:id="ahsh-l1p1i1t5s2q1c7-d1e3747">
          <lb ed="#Q"/>Consequenter quaeritur utrum praescientia sit
          <lb ed="#Q"/>mutabilis.
        </p>
        <p xml:id="ahsh-l1p1i1t5s2q1c7-d1e3754">
          <lb ed="#Q"/>Ad quod. sic arguunt: Deus potest praescire
          <lb ed="#Q"/>quodk non praescivit, quia potest facere aliquod
          <lb ed="#Q"/>futurum, quod non erit futurum; ergo potest ali<lb ed="#Q"/>quid
          scire de novo et temporaliter; ergol muta<lb ed="#Q"/>biliter.
        </p>
        <p xml:id="ahsh-l1p1i1t5s2q1c7-d1e3767">
          <lb ed="#Q"/>Ad quod datur responsio talis in Sententiisz:
          <lb ed="#Q"/>quod prima propositio divisa est vera, composita '"
          <lb ed="#Q"/>falsa. Nam ista duo simul non possunt esse: ' Deum
          <lb ed="#Q"/>non praescivisse seu scivisse aliquid' et 'illud scire
          <lb ed="#Q"/>vel praescire'; potest tamen modo iacere quod est
          <lb ed="#Q"/>futurum, quod tamen non est praescitum, et sic
          <lb ed="#Q"/>potest illud praescire. — Alii distinguunt illam "
          <lb ed="#Q"/>propositionem: nam haec dictio 'quod' potest
          <lb ed="#Q"/>notare coniunctionem sive comparationem actus
          <lb ed="#Q"/>ad actum vel actus ad potentiam. Si- notet con<lb ed="#Q"/>iunctionem
          actus ad actum,, videlicet eius quod
          <lb ed="#Q"/>dico ' scire aliquid ' ad hoc quod dico ' non " sci<lb ed="#Q"/>visse
          ', falsa est, quia non est possibile Deum
          <lb ed="#Q"/>scire aliquid et non scivisse illud. Si veronotet
          <lb ed="#Q"/>coniunctionem actus ad potentiam", ut eius. quod
          <lb ed="#Q"/>dico 'non scivisse aliquid' ad hoc quod dico
          ' posse scire illud ', veraP, quia haec bene sese
          <lb ed="#Q"/>compatiuntur 'Deum non scire aliquid', proutq
          <lb ed="#Q"/>scientia dicitur visio, et ' Deum posse scire illud ',
          <lb ed="#Q"/>quia potest facere aliquid quod non faciet, et ita
          <cb ed="#Q" n="b"/>
          <lb ed="#Q"/>potest scire quod non scivit, id est quod non
          <lb ed="#Q"/>praevidit' se facturum.
        </p>
        <p xml:id="ahsh-l1p1i1t5s2q1c7-d1e3818">
          <lb ed="#Q"/>Secundum ergo<!--s--> hunc modum,- ut accipi-atur
          <lb ed="#Q"/>propositio in sensu in' quo vera est, quaeritur an
          <lb ed="#Q"/>sequatur: ' Deus potest praescire vel scire aliquid
          quod non scivit vel praescivit ', ergo potest ali<lb ed="#Q"/>quid 
          scire vel praescire de novo vel ex tempore'.
        </p>
        <p xml:id="ahsh-l1p1i1t5s2q1c7-d1e3831">
          <lb ed="#Q"/>Probatur quod sic: I. Quia bene sequitur:
          'Deus creat modo aliquid quod prius- non crea<lb ed="#Q"/>vit,
          ergo creat aliquid de novo vel ex tempore';
          <lb ed="#Q"/>ergo similiter sequitur: 'Deus scit aliquid vel
          <lb ed="#Q"/>praescii. modo quod prius non scivit vel prae<lb ed="#Q"/>scivit,
          ergo scit aliquid vel" praescit de novo
          <lb ed="#Q"/>vel ex tempore'; ergo similiter sequitur: si po<lb ed="#Q"/>test
          aliquid scire quod prius non scivit, quod'
          <lb ed="#Q"/>potest aliquid sci-re de novo vel ex tempore,
          <lb ed="#Q"/>quia si actus sequitur ad actum, et potentia se<lb ed="#Q"/>quiturx
          ad potentiam.
        </p>
        <p xml:id="ahsh-l1p1i1t5s2q1c7-d1e3855">
          <lb ed="#Q"/>2. Item, in scientia hominis simul est verum
          <lb ed="#Q"/>praescire hoc et' non praescivisse illud; ergo et
          <lb ed="#Q"/>in scientia Dei simul potest esse verum, Deum
          <lb ed="#Q"/>praescire aliquid et non praescivisse illud; ergo?
          <lb ed="#Q"/>non est vera praedicta: solutio quantum ad unam
          <lb ed="#Q"/>partem. '
        </p>
        <p xml:id="ahsh-l1p1i1t5s2q1c7-d1e3871">
          <lb ed="#Q"/>3. Item, quantum ad aliam in qua dicitur quod
          <lb ed="#Q"/>divisa est vera, quia Deus potest praescirez ali<lb ed="#Q"/>quid
          quod tamen non est verum eum praesci<lb ed="#Q"/>visse,
          et ita contingens est ipsum praescivisse,
          <pb ed="#Q" n="273"/>
          <cb ed="#Q" n="a"/>
          <lb ed="#Q"/>obicitur: omne dictum de praeterito verum
          <lb ed="#Q"/>est necessarium; ergo, cum, demonstrato quolibet
          <lb ed="#Q"/>futuro contingente, ' Deum praescivisse hoc ' sit"
          <lb ed="#Q"/>dictum de praeterito verumb est necessarium;
          <lb ed="#Q"/>ergo * non potest non praesciri a Deo. Relinqui<lb ed="#Q"/>tur
          igitur quod non potest non d praescire quod
          <lb ed="#Q"/>praesciverit. — Contra: Quod est praescitum
          <lb ed="#Q"/>potest non esse futurum, et quod potest non esse
          <lb ed="#Q"/>futurum potest non esse praescitum, quia conco<lb ed="#Q"/>mitantur
          se invicem 'praescitum' et 'esse tu<lb ed="#Q"/>turum'
          ; ergo Deum praescivisse hoc, non est
          <lb ed="#Q"/>necessarium. . . ,
        </p>
        <p xml:id="ahsh-l1p1i1t5s2q1c7-d1e3913">
          <lb ed="#Q"/>4. Item, quaeritur utrum haec possit esse vera
          ' Deus potest habere scientiam quam nunquam
          <lb ed="#Q"/>habuit ', sicut haec conceditur in uno sensu ' po<lb ed="#Q"/>test
          scire quod nunquam scivit *, cum scire et
          <lb ed="#Q"/>habere scientiam idem sit. Quod si dicatur— co n<lb ed="#Q"/>tra:
          si Deus potest habere scientiam quam nun<lb ed="#Q"/>quam
          habuit, ergo augmentati potest eius scientia.
        </p>
        <p xml:id="ahsh-l1p1i1t5s2q1c7-d1e3929">
          <lb ed="#Q"/>[Solutio]:- Circa hoc notandum est quod ad
          <lb ed="#Q"/>hoc argumentum 1 « ' Deus immutabiliter praescit
          <lb ed="#Q"/>istum damnandum esse, ergo necessario praevidit
          <lb ed="#Q"/>istum esse damnandum, ergo necessarium est
          <lb ed="#Q"/>istum damnari ', quidam e respondebant sic dis<lb ed="#Q"/>tinguendo
          'Deus immutabiliter praevidet hoc':
          <lb ed="#Q"/>quia si haec determinatio 'immutabiliter' deter<lb ed="#Q"/>minet
          hoc verbum 'praevidere' in comparatione
          <lb ed="#Q"/>ad nominativum, verum est, quia in Deo nulla
          <lb ed="#Q"/>est mutatio; si in comparatione ad rem accusa<lb ed="#Q"/>tivi,
          falsa est, quia in re visa est mutabilitas ».
        </p>
        <p xml:id="ahsh-l1p1i1t5s2q1c7-d1e3956">
          <lb ed="#Q"/>Quam solutionem nituntur2 improbare
          <lb ed="#Q"/>hac ratione: « Sicut non potest esse quod Deus
          <lb ed="#Q"/>praesciat aliquid quod priusf non praesciverit, ita
          <lb ed="#Q"/>non potest esse quod aliquid sit modo praevisum,
          <lb ed="#Q"/>quod prius non fuerit praevisum, et ita immuta<lb ed="#Q"/>bilitas
          est ex parte rei praevisae in quantum
          <lb ed="#Q"/>praevisa est; oportet autem immutabilitatem esse
          <lb ed="#Q"/>rei praeuisae, cum hoc adverbium 'immutabili—
          <lb ed="#Q"/>ter' determinat verbum in comparatione ad ac<lb ed="#Q"/>cusativum
          ». Unde concedunt quod « utroque
          <lb ed="#Q"/>modo vera sit ». — Et dicunt « non sequi: 'im<lb ed="#Q"/>mutabiliter
          praevidet hoc, ergo necessario prae<lb ed="#Q"/>videt
          vel praescit hoc', immo est fallacia conse<lb ed="#Q"/>quentis.
          Non enim omne immutabile verum est
          <lb ed="#Q"/>necessarium, sed e contrario: unde ' Antichristum
          <lb ed="#Q"/>fuisse futurum' est verum immutabile, non ta<lb ed="#Q"/>men
          necessarium ». Praeterea, addunt quod « Deus
          <lb ed="#Q"/>praevidet res quales futurae sunt: si necessariae,
          <lb ed="#Q"/>praevidet eas in necessitate; si contingentes,
          <cb ed="#Q" n="b"/>
          <lb ed="#Q"/>praevidet in contingentia; unde si praevidet con<lb ed="#Q"/>tingens
          iuturum, contingens est Deum praevidisse '
          <lb ed="#Q"/>illud ». -— Item, addunt quod si « quaeritur in quo
          <lb ed="#Q"/>sit illa contingentia », dicunt « quod in Deo, et
          <lb ed="#Q"/>non est aliud quam potentia praevidendi, quae
          <lb ed="#Q"/>est in Deo secundum quod se habet ad duo op<lb ed="#Q"/>posita:
          ' praevidere' et 'non praevidere'g ». —
          [tem, addunt-ad illud quod aliquis obiceret: « si
          <lb ed="#Q"/>homo scivit aliquid, necessarium est ipsum sci<lb ed="#Q"/>visse;
          ergo cum certior sit'! scientia Dei quam
          <lb ed="#Q"/>scientia hominis, si Deus praevidit aliquid, ne<lb ed="#Q"/>cessarium
          est Deum praevidisse illud », et re<lb ed="#Q"/>spondent
          quod « scientia Dei est certiori scientia
          <lb ed="#Q"/>hominis quantum ad intallibilitatem, sed non
          <lb ed="#Q"/>quantum ad rei fixionem vel determinationem,
          <lb ed="#Q"/>quia non generatur a rebus ».
        </p>
        <p xml:id="ahsh-l1p1i1t5s2q1c7-d1e4034">
          <lb ed="#Q"/>Sed istae sententiae quia videntur inducere
          <lb ed="#Q"/>non sanam fidei confessionem 3, ideo attendendum
          <lb ed="#Q"/>est primo quod sophistice obicitur contra sol<lb ed="#Q"/>ventes
          praedictum sophisma 'Deus immutabiliter
          <lb ed="#Q"/>praevidet hoc, scilicet istum esse damnandum '.
          <lb ed="#Q"/>Nam illorum intentio non est dicere quod muta<lb ed="#Q"/>bilitas
          sit ex parte rei.in quantum praevisa est,
          <lb ed="#Q"/>sed solum in quantum futura est. Et hoc importat
          <lb ed="#Q"/>ipsum verbum ' praevidere ', si resolvatur secun<lb ed="#Q"/>dum
          An selmu m 4, scilicet ' videre futurum ', et
          <lb ed="#Q"/>si compleatur sensus 'videre futurum esse'. Si
          <lb ed="#Q"/>autem sic explicite diceretur: ' Deus immutabiliter
          <lb ed="#Q"/>videt vel scit esse vel fore futurum illud', nullus
          <lb ed="#Q"/>dubitaret distinguendam esse locutionem: nam
          <lb ed="#Q"/>hoc adverbium 'immutabiliter' posset ferri ad
          <lb ed="#Q"/>hoc * verbum ' videre ', et hoc modo vera; vel ad
          <lb ed="#Q"/>hoc verbum 'esse vel' fore ', et hoc modo falsa,
          <lb ed="#Q"/>quia sic designatur immutabilitas ex parte rei fu<lb ed="#Q"/>turae,
          non ex parte videntis. In hoc autem quod
          <lb ed="#Q"/>dicitur 'praevidere hoc ' totum includitur, scilicet
          ' videre futurum esse," vel fore '. Sophistica igitur
          <lb ed="#Q"/>est contra dictam solutionem oppositio.
        </p>
        <p xml:id="ahsh-l1p1i1t5s2q1c7-d1e4082">
          <lb ed="#Q"/>Item, quod secundo dicunt quod ' non omne "
          <lb ed="#Q"/>immutabile verum est necessarium', hoc videtur
          <lb ed="#Q"/>manifeste falsum. Nam contingens verum non
          <lb ed="#Q"/>potest esse immutabile verum: contingens enim
          <lb ed="#Q"/>ex eo esto quod potest esse et non esse; sed
          <lb ed="#Q"/>omneP tale est mutabile; omne ergo contingens
          <lb ed="#Q"/>verum est verum mutabile; ergo si est immu<lb ed="#Q"/>tabile
          verum, non est verum. contingens. Cum
          <lb ed="#Q"/>ergo omne quod est verum, est necessarium vel
          <lb ed="#Q"/>contingens, si est immutabile verum, est neces<lb ed="#Q"/>sarium.
          — Item, cum omnis contingentia ponat
          <pb ed="#Q" n="274"/>
          <cb ed="#Q" n="a"/>
          <lb ed="#Q"/>mutabilitatem <!--a--> ad esse et non-esse, sicut impos<lb ed="#Q"/>'
          sibilitasb immutabilitatem in non-esse, necessitas
          <lb ed="#Q"/>immutabilitatem in esse: si non est sana enun<lb ed="#Q"/>tiatio
          qua diceretur mutabilitas in Deo, ergo non
          <lb ed="#Q"/>erit sana locutio qua dicitur contingentia in Deo,
          <lb ed="#Q"/><quote xml:id="ahsh-l1p1i1t5s2q1c7-Qd1e4126" ana="#iac1_17">apud quem non est transmutatio nec vicissitudinis
          <lb ed="#Q"/>obumbratio</quote>, <ref>Iac. 1,17</ref>.
        </p>
        <p xml:id="ahsh-l1p1i1t5s2q1c7-d1e4135">
          <lb ed="#Q"/>Item, quod dicitur quod 0 ' illa contingentia non
          <lb ed="#Q"/>est nisi indifferentia potentiae praevidendi et non
          <lb ed="#Q"/>praevidendi ', non videtur recte dictum. Nam quid
          <lb ed="#Q"/>est aliud potentia praevidendi quam potentia vi—
          <lb ed="#Q"/>dendi futurum, vel, completiori intellectu, potentia
          <lb ed="#Q"/>videndi aliquid fore vel futurum esse? Sed con<lb ed="#Q"/>stat
          quod non dicitur" indifferentia potentiae ra<lb ed="#Q"/>tione
          visionis, quae non potest non esse quan<lb ed="#Q"/>tum
          est ex parte videntis; ergo dicetur indit<lb ed="#Q"/>ferentia
          ratione visionis in quantum transit super
          <lb ed="#Q"/>rem futuram, quae potest esse et non esse et ideo
          <lb ed="#Q"/>potest esse subiecta visioni vel non esse subiecta;
          <lb ed="#Q"/>sed secundum hoc nulla ponitur contingentia in
          <lb ed="#Q"/>actu visionis, sed in subiecto ipsi visioni. Et
          <lb ed="#Q"/>est exemplumjl in illuminatione solis 1. Relinquitur
          <lb ed="#Q"/>ergo quod contingentia illa non ponitur in Deo
          <lb ed="#Q"/>ut potentia praevidendi et non praevidendi.
        </p>
        <p xml:id="ahsh-l1p1i1t5s2q1c7-d1e4174">
          <lb ed="#Q"/>Item; ad illud quod dicunt quode 'certior est
          <lb ed="#Q"/>scientia humana quantum ad rei fixionem vel de<lb ed="#Q"/>terminationem
          f, quamvis non quantum ad infal<lb ed="#Q"/>libilitatem
          ', nullo modo videtur dicendum, quia
          <lb ed="#Q"/>ex hoc sequeretur quod aliqua certitudine exce<lb ed="#Q"/>deret
          scientia humana divinam, quod est error.
          <lb ed="#Q"/>Propterea, cum ipsa divina intelligentia magis sit
          <lb ed="#Q"/>intima "rebus'quam humana, quia in omnibus es<lb ed="#Q"/>sentialiter
          est et omnia sunt in ipso causaliter,
          <lb ed="#Q"/>etiam quantum ad rerum fixionem vel determina<lb ed="#Q"/>tionem
          certior est, quamvis nullo modo sit a rebus.
        </p>
        <p xml:id="ahsh-l1p1i1t5s2q1c7-d1e4200">
          <lb ed="#Q"/>Item, in hoc fuit deceptio considerationis, quod
          <lb ed="#Q"/>cum dicitur ' Deum praescivisse aliquid ', duo
          <lb ed="#Q"/>asseruntur: unum de praeterito, scilicet scientiam
          <lb ed="#Q"/>fuisse, et aliud de futuro, scilicet scitum futurum
          <lb ed="#Q"/>esse, et tameng uno dicto utrumque asseritur.
          <lb ed="#Q"/>Quia ergo contingentia est ex parte sciti, quamvis
          <lb ed="#Q"/>non in quantum scitum est, sed in quantum" fu<lb ed="#Q"/>turum,
          ideo ipsum dictum contingenter est verum.
          <lb ed="#Q"/>Et similiter contingit in ipsa praescientia humana,
          <lb ed="#Q"/>quia licet, antequam res contingens futura eve<lb ed="#Q"/>niat,
          mutabilis est et potest evenire et non eve<lb ed="#Q"/>nire,
          sicut in praescientia prophetali, non sequitur
          <lb ed="#Q"/>quod praescientia sit mutabilis. Nam secundum
          <lb ed="#Q"/>quod est in praescientia, ipsum futurum est im"<lb ed="#Q"/>mutabiliteri
          ; hoc tamen non est simpliciter, sicut
          <lb ed="#Q"/>dixerunt, sed solum secundum quid.
        </p>
        <p xml:id="ahsh-l1p1i1t5s2q1c7-d1e4238">
          <lb ed="#Q"/>Dicendum igitur secundum <name ref="#Anselm">B. Anselmum</name>, in 
          <lb ed="#Q"/>libro <title>De concordia praescientiae et liberi arbitrii</title>z:
          <quote xml:id="ahsh-l1p1i1t5s2q1c7-Qd1e4250">« Deus non fallitur nec videt nisi veritatem, sive
          <lb ed="#Q"/>ex libertate sive ex necessitate veniat, dicitur
          <lb ed="#Q"/>constituisse apud se immutabiliter quod apud
          <lb ed="#Q"/>hominem *, prius quam fiat, mutari potest »</quote>: non
          <lb ed="#Q"/>tamen in Deo, qui omnino immutabilis est, cadit'
          <lb ed="#Q"/>mutatio.
        </p>
        <p xml:id="ahsh-l1p1i1t5s2q1c7-d1e4265">
          <lb ed="#Q"/>[Ad obiecta]: 1. Respondendum autem ad pri<lb ed="#Q"/>mum
          quod non est similis ratio in hoc verbo
          'scire vel praescire' et in hoc verbo ' creare '..
          <lb ed="#Q"/>Nam hoc verbum 'creare' connotat effectum in
          <lb ed="#Q"/>creatura in "' tempore, hoc verbum vero ' scire vel
          <lb ed="#Q"/>praescire ' non connotat " effectum sive respectum
          <lb ed="#Q"/>secundum actum in tempore. Quamvis ' praescire '
          <lb ed="#Q"/>dicatur respectu futuri in tempore, tamen respec<lb ed="#Q"/>tus
          ille non est temporalis; et inde est quod
          'scire vel praescire' dicitur de Deo ab aeterno,
          'creare' vero ex tempore. Unde notandum est
          <lb ed="#Q"/>quod, quamvis ' scire et praescire et creare ' haec
          <lb ed="#Q"/>verba dicant respectum ad creaturam, cum di<lb ed="#Q"/>citur
          ' Deus scit res, praescit res, creat res ', ta<lb ed="#Q"/>men
          differenter dicunt, quia hoc quod dico 'scire'
          <lb ed="#Q"/>dicit respectum ad res secundum esse in po<lb ed="#Q"/>tentia,
          hoc quod dico 'praescire'0 dicit respec<lb ed="#Q"/>tum
          ad res secundum esseP in praeordinatione,
          <lb ed="#Q"/>hoc quod dico 'creare' dicit respectum ad res
          <lb ed="#Q"/>secundum esse in actu. Hinc est quod praescientia
          <lb ed="#Q"/>vel scientia non ponit res in esse secundum tem<lb ed="#Q"/>poralem
          existentiam, sed ponit secundum quod
          <lb ed="#Q"/>sunt per praevisionem in causa ; creatio vero ponit
          <lb ed="#Q"/>res 9 actu, secundum quod actualiter sunt a causa.
          <lb ed="#Q"/>Et ideo sequitur: ' creat modo aliquid quod prius
          <lb ed="#Q"/>non creavit, ergo creat'" aliquid ex tempore',
          <lb ed="#Q"/>non tamen sequitur cum hoc verbo 'scire'; et
          <lb ed="#Q"/>similiter sequitur * cum 'potentia': ' potest aliquid
          <lb ed="#Q"/>creare quod prius non creavit, ergo potest ali<lb ed="#Q"/>quid
          creare ex tempore '; non tamen sequitur cum
          <lb ed="#Q"/>hoc verbo 'sciref vel praescire': 'potest scire
          <lb ed="#Q"/>vel praescire" quod prius non scivit vel praesci<lb ed="#Q"/>vit
          ", ergo potest aliquid scire vel praescire ex
          <lb ed="#Q"/>tempore'.
        </p>
        <p xml:id="ahsh-l1p1i1t5s2q1c7-d1e4335">
          <lb ed="#Q"/>2. Ad secundum dicendum quod non est
          <lb ed="#Q"/>simile de praescientia Dei et de praescientia * ho<lb ed="#Q"/>minis.
          Nam cum dicitur 'praescire' de homine,
          <lb ed="#Q"/>notatur ordo rei temporalis ad tempus praecedens;
          <lb ed="#Q"/>sed antecessio temporis ad tempus non est infi<lb ed="#Q"/>nita
          ? ; et ideo dicendum quod non sequitur: ' homo
          <lb ed="#Q"/>praescit hoc 2, ergo prius praescivit illud '. Sed cum
          dicitur ' praescire ' de Deo, notatur ordo rei tempo<lb ed="#Q"/>ralis 
          ad aeternitatem; antecessio autem aeternita<lb ed="#Q"/>tis
          ad tempus est infinita; et ideo sequitur: ' Deus
          <lb ed="#Q"/>praescit hoc, ergo praescivit hoc prius'.
        </p>
        <p xml:id="ahsh-l1p1i1t5s2q1c7-d1e4359">
          <pb ed="#Q" n="275"/>
          <cb ed="#Q" n="a"/>
          <lb ed="#Q"/>3. Ad tertium dicendum quod" cum dico
          'praescivisse ', ratione ipsius verbi consignifico
          <lb ed="#Q"/>tempus praeteritum, ratione ipsius praepositionis
          'prae' consignifico futurum; unde 'Deum prae<lb ed="#Q"/>scivisse'
          nihil aliud est quam scivisse hoc esse
          <lb ed="#Q"/>futurum. Unde patet quod hoc dictum non est
          <lb ed="#Q"/>simpliciter de praeterito, sed est de praeterito re<lb ed="#Q"/>spectu
          futuri; et ideob quia futurum est contin<lb ed="#Q"/>gens,
          respectus ' ipsius scientiae, quae significatur 
          <lb ed="#Q"/>praeterite, contingens est. Unde quamvis ipsa<lb ed="#Q"/>
          <lb ed="#Q"/>scientia sit necessaria, tamen, quia respectus ad
          <lb ed="#Q"/>futurum est contingens, ideo ipsum dictum de
          <lb ed="#Q"/>praeterito, quod dicit ipsum respectum ad futu<lb ed="#Q"/>rum
          contingens, erit contingens, non necessarium.
          — Tamen ista posset distingui ' Deum praescivisse
          <lb ed="#Q"/>hoc est necessarium' quemadmodum et haec in
          <lb ed="#Q"/>quam resolvitur 'Deum scivisse hoc esse futu<lb ed="#Q"/>rum
          est necessarium '. Narn necessitas potest no<lb ed="#Q"/>tari
          respectu huius verbi 'esse', quod intelligitur
          <lb ed="#Q"/>cum dicitur 'necessarium est Deum scivisse hoc
          <lb ed="#Q"/>esse futurum sive ** hoc fore ', et hoc modo falsa
          <lb ed="#Q"/>est ', quia tunc est sensus: 'Deus scivit hoc ne<lb ed="#Q"/>cessario
          esse futurum sive necessario iore'; vel
          <lb ed="#Q"/>respectu huius verbi ' praescire ', et sic est vera,
          <lb ed="#Q"/>et est sensus: 'scientia Dei, quae est respectu
          <lb ed="#Q"/>futuri, est necessaria'.
        </p>
        <p xml:id="ahsh-l1p1i1t5s2q1c7-d1e4418">
          <lb ed="#Q"/>4. Ad quartum dicendum quod huiusmodi
          <cb ed="#Q" n="b"/>
          <lb ed="#Q"/>locutiones non sunt "eiusdem virtutis 'Deus po<lb ed="#Q"/>test
          scire quod non scivit' et ' Deusf potest ha<lb ed="#Q"/>bere
          scientiam quam nunquam habuit '. Cum enim
          <lb ed="#Q"/>dicitur 'Deus potest scire quod non scit ', po<lb ed="#Q"/>tentia
          transit in rem temporalem, ratione cuius
          <lb ed="#Q"/>temporalis conceditur haec locutio: res enim tem<lb ed="#Q"/>poralis,
          licet futura sit, potest tamen! non esse.
          <lb ed="#Q"/>Cum autemh dicitur 'Deus potest habere scien<lb ed="#Q"/>tiam
          quam non habet', notatur respectus poten<lb ed="#Q"/>tiae
          ad actum transeuntem in rem aeternam:
          <lb ed="#Q"/>scientia enim Dei aeterna est, quae nunquam ab
          <lb ed="#Q"/>eo separatur; unde quia tangitur ibi temporale
          <lb ed="#Q"/>respectu scientiae vel actus aeterni, non est con<lb ed="#Q"/>cedenda.
        </p>
        <p xml:id="ahsh-l1p1i1t5s2q1c7-d1e4455">
          <lb ed="#Q"/>Item, obicitur, quiai videtur illa negari simpli<lb ed="#Q"/>citer
          a <name ref="#Lombard">Magistro</name> 'Deus potest aliquid scire
          <lb ed="#Q"/>ex tempore' - contra: Deus scit et potest ali<lb ed="#Q"/>quid
          scire quod incipit esse ex tempore; ergo po<lb ed="#Q"/>test
          aliquid scire ex tempore. -— Respondeo:
          <lb ed="#Q"/>Haec determinatio ' ex tempore' potest deter<lb ed="#Q"/>minare
          hoc verbum 'potest ', et sic est falsa,
          <lb ed="#Q"/>quia sensus est: 'Deus potest ex tempore scire
          <lb ed="#Q"/>aliquid, quod non scivit ab aeterno'; vel potest
          <lb ed="#Q"/>determinare hoc verbum ' scire' in comparatione
          <lb ed="#Q"/>ad accusativum super quem transit, scilicet 'ali<lb ed="#Q"/>quid',
          et hoc modo vera est, et est sensus: ' Deus
          <lb ed="#Q"/>potest aliquid scire quod existit in tempore".
        </p>
      </div>
    </body>
  </text>
</TEI>